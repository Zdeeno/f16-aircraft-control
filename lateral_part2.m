%% Lateral states
%
% lateral linearization
% x = [
%   phi     [rad]   roll angle
%   psi     [rad]   yaw angle
%   v       [ft/s]
%   beta    [rad]   angle of slide-slip
%   p       [rad/s] roll rate
%   r       [rad/s] yaw rate
%   delta_t [-]
%   delta_a [-]
%   delta_r [-]
% ]
% 
% input
% u = [
%   delta_t
%   delta_a
%   delta_r
% ]

A = [      0         0         0         0    1.0000    0.0749         0         0         0;
            0         0         0         0         0    1.0028         0         0         0;
            0         0   -0.0119   -0.1873         0         0    0.0016         0         0;
       0.0802         0   -0.0000   -0.1754    0.0752   -0.9913         0    0.0002    0.0005;
            0         0    0.0000  -16.7231   -2.0988    0.4939         0   -0.3229    0.0610;
            0         0   -0.0000    3.6584   -0.0373   -0.2957         0   -0.0136   -0.0303;
            0         0         0         0         0         0   -1.0000         0         0;
            0         0         0         0         0         0         0  -20.2000         0;
            0         0         0         0         0         0         0         0  -20.2000]
        
B = [      0         0         0;
            0         0         0;
            0         0         0;
            0         0         0;
            0         0         0;
            0         0         0;
       1.0000         0         0;
            0   20.2000         0;
            0         0   20.2000]

C = [57.2958       0      0         0         0         0 0 0 0;
            0 57.2958      0         0         0         0 0 0 0;
            0       0 1.0000         0         0         0 0 0 0;
            0       0      0   57.2958         0         0 0 0 0;
            0       0      0         0   57.2958         0 0 0 0;
            0       0      0         0         0   57.2958 0 0 0]
        
D = zeros(6,3)

K = [ 0.0000   -0.0004    0.0000   -0.0004    0.0000   -0.0000    0.0000   -0.0000    0.0000   -0.0000   -0.0000;
  -24.3057  -21.4877   -0.0000   12.4827   -4.8810  -13.2842   -0.0000    0.1788    0.0095   30.5538    8.1525;
    4.1462 -107.6069    0.0002 -103.2043    0.0897   -8.6563    0.0000    0.0095    0.0257   -8.1525   30.5538]

% create and control augmented system
augC = [1 0 0 0 0 0 0 0 0 0 0;
        0 1 0 0 0 0 0 0 0 0 0];
augB = [B; [0 0 0; 0 0 0]];
augA = [A zeros(9,2); -augC];
augD = [0 0 0; 0 0 0];


controlB = [0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            1 0;
            0 1];
controlD = [0 0;
            0 0];
controlA = augA - augB*K;
controlsys = ss(controlA, controlB, augC, controlD)

