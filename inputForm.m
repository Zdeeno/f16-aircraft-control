function ret = inputForm(A)
% Displays matrix in matlab input form

ret = newline + "[";
for i = 1:size(A, 1)
    ret = ret + string(A(i, :)).join();
    ret = ret + ";" + newline;
end
ret = char(ret);
ret(end - 1) = "]";
disp(ret)