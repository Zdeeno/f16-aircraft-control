%% SIMULINK
% longitudal

A = [0 510 0 -510 0 0 0;
     0 0 0 0 1 0 0;
     0.000103945 -32.17 -0.0130211 2.60145 -0.987486 0.00156714 0.0674011;
     1.962e-06 -4.6226e-13 -0.0002458 -0.8061 0.92912 -1.8584e-07 -0.0017586;
     8.3982e-12 0 -1.052e-09 -0.6446 -1.0433 0 -0.14595;
     0 0 0 0 0 -1 0;
     0 0 0 0 0 0 -20.2];
 
B = [0 0;
     0 0;
     0 0;
     0 0;
     0 0;
     1 0;
     0 20.2];
 
C = [1 0 0 0 0 0 0;
     0 57.2958 0 0 0 0 0;
     0 0 1 0 0 0 0;
     0 0 0 57.2958 0 0 0;
     0 0 0 0 57.2958 0 0];
 
D = [0 0;
     0 0;
     0 0;
     0 0;
     0 0];
 
Slong.A = A;
Slong.B = B;
Slong.C = C;
Slong.D = D;
Slong.C_eye = eye(7);
Slong.D_eye = zeros(7, 2);

% lateral
A = [0 0 0 0 1 0.036082 0 0 0;
     0 0 0 0 0 1.0007 0 0 0;
     0 0 -0.012196 -0.23674 0 0 0.001569 0 0;
     0.053582 0 -1.0278e-06 -0.26572 0.035742 -0.99366 0 0.00026058 0.00073309;
     0 0 2.03643e-10 -30.7887 -3.20706 0.582874 0 -0.720266 0.138067;
     0 0 -2.17e-08 8.4931 -0.021705 -0.44063 0 -0.031442 -0.068146;
     0 0 0 0 0 0 -1 0 0;
     0 0 0 0 0 0 0 -20.2 0;
     0 0 0 0 0 0 0 0 -20.2];
 
B = [0 0 0;
     0 0 0;
     0 0 0;
     0 0 0;
     0 0 0;
     0 0 0;
     1 0 0;
     0 20.2 0;
     0 0 20.2];
 
C = [57.2958 0 0 0 0 0 0 0 0;
     0 57.2958 0 0 0 0 0 0 0;
     0 0 1 0 0 0 0 0 0;
     0 0 0 57.2958 0 0 0 0 0;
     0 0 0 0 57.2958 0 0 0 0;
     0 0 0 0 0 57.2958 0 0 0];
 
D = [0 0 0;
     0 0 0;
     0 0 0;
     0 0 0;
     0 0 0;
     0 0 0];
 
Slat.A = A;
Slat.B = B;
Slat.C = C;
Slat.D = D;
Slat.C_eye = eye(9);
Slat.D_eye = zeros(9, 3);

clear A B C D
%% Longitudal state integral control
%
% longitudal linearization
% x = [
%   h         [ft]    - altitude
%   theta     [rad]   - pitch angle
%   v         [ft/s]  - velocity
%   alha      [rad]   - angle of attack
%   q         [rad/s] - pitch rate
%   delta_t   [-]
%   delta_e   [-]
% ]
% 
% input
% u = [
%   u_delta_t
%   u_delta_e
% ]

A = Slong.A;
B = Slong.B;
C = Slong.C;

D = Slong.D;

initsys = ss(A, B, C, D);
damp(initsys)

pitch_idx = 2;
pitch_rate_idx = 5;
elevator_idx = 7;

% create augmened system
augC = [0 1 0 0 0 0 0 0 0;
        0 0 1 0 0 0 0 0 0];
augB = [B; [0 0;0 0]];
augA = [A zeros(7,2); -augC];
augD = [0 0; 0 0];

augsys = ss(augA, augB, augC, augD);

%augsys = ss(transpose(augA), transpose(augC), transpose(augB), augD);

% find control for augemnted sys using LQR
Q = diag([0, 0, 10, 10, 0, 1, 1000, 1000, 3]); 
R = diag([1, 1]);
K = lqr(augsys, Q, R)

Slong.Ke = K(:, 1:7);
Slong.Ki = K(:, 8:9);
Slong.ref_pitch = 0.1; % rad

%%

% crete new system with controler
controlB = [0; 0; 0; 0; 0; 0; 0; 1];
controlD = 0;
controlA = augA - augB*K;
controlsys = ss(controlA, controlB, augC, controlD);

% show results
step(controlsys)
grid on
damp(controlsys)
figure
T = 0:0.1:400;
input = ones(size(T)) * 0.1;
hold on
[y, t, x] = lsim(controlsys, input, T);
plot(t, x(:, pitch_idx), "-r");
plot(t, x(:, pitch_rate_idx), "-b");
plot(t, x(:, elevator_idx), "-g");
title("Pitch reference tracking")
grid on


%% Lateral state integral control
%
% lateral linearization
% x = [
%   phi     [rad]   roll angle
%   psi     [rad]   yaw angle
%   v       [ft/s]
%   beta    [rad]   angle of slide-slip
%   p       [rad/s] roll rate
%   r       [rad/s] yaw rate
%   delta_t [-]
%   delta_a [-]
%   delta_r [-]
% ]
% 
% input
% u = [
%   delta_t
%   delta_a
%   delta_r
% ]

A = Slat.A;
B = Slat.B;
C = Slat.C;
D = Slat.D;

roll_idx = 1;
yaw_idx = 2;
roll_rate_idx = 5;
yaw_rate_idx = 6;
aileron_idx = 8;
rudder_idx = 9;
 
initsys = ss(A, B, C, D);
damp(initsys)

% create and control augmented system
augC = [1 0 0 0 0 0 0 0 0 0 0 0;
        0 1 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 0 0 0 0 0 0]
augB = [B; [0 0 0; 
            0 0 0;
            0 0 0]]
augA = [A zeros(9,3); -augC]
augD = [0 0 0; 
        0 0 0;
        0 0 0]

augsys = ss(augA, augB, augC, augD);

Q = diag([0, 0, 0, 0, 0, 0, 0, 0, 0, 1000, 1000, 1000]); 
R = diag([1, 1, 1]);
K = lqr(augsys, Q, R)

Slat.Ka = K(:, 1:9);
Slat.Ki = K(:, 10:12);

Slat.ref_yaw = 0.1;
Slat.ref_roll = 0.2;

%%
% crete new system with controler
controlB = [0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            0 0;
            1 0;
            0 1];
controlD = [0 0;
            0 0];
controlA = augA - augB*K;
controlsys = ss(controlA, controlB, augC, controlD);

% show results
damp(controlsys)
T = 0:0.1:30;

% yaw control
input = zeros(2, numel(T));
input(yaw_idx, :) = 0.1;
[~, t, x] = lsim(controlsys, input, T);
figure
hold on
plot(t, x(:, yaw_idx), "-r");
plot(t, x(:, yaw_rate_idx), "-b");
plot(t, x(:, rudder_idx), "-g");
title("Yaw reference tracking")
grid on

% roll control
input = zeros(2, numel(T));
input(roll_idx, :) = 0.1;
[~, t, x] = lsim(controlsys, input, T);
figure
hold on
plot(t, x(:, roll_idx), "-r");
plot(t, x(:, roll_rate_idx), "-b");
plot(t, x(:, aileron_idx), "-g");
title("Roll reference tracking")
grid on